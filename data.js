export const menuArray = [
    {
        name: "Pad Thai",
        ingredients: ["rice noodles", "dried shrimp", "sauce of tamarind", "tofu", "vegetables"],
        price: 5,
        emoji: "🍕",
        id: 0
    },
    {
        name: "Tom Yum",
        ingredients: ["shrimp", "tom yum sauce", "mushrooms"],
        price: 6,
        emoji: "🍔",
        id: 1
    },
        {
        name: "Coconut Water",
        ingredients: ["coconut water"],
        price: 3,
        emoji: "🍺",
        id: 2
    }
]