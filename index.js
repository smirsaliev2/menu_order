import { menuArray } from './data.js';

const orderMap = new Map();

document.addEventListener('click', function(e) {
    if (e.target.dataset.addDish) {
        addToOrderMap(e.target.dataset.addDish);
    }
    else if(e.target.id.startsWith('decrement-')) {
        decrementInOrderMap(e.target.id)
    }
    else if(e.target.dataset.removeDish) {
        removeDishOrder(e.target.dataset.removeDish)
    }
    else if(e.target.id === 'complete-order-btn') {
        togglePaymentModal();

    }
    else if(e.target.id === 'pay-btn') {
        acceptPayment(e);
    }
    else if(e.target.id === 'close-pay-btn') {
        togglePaymentModal();
    }
})

// Logs out pay form data and order details.
// Renders initial state.
function acceptPayment(e) {
    const payForm = document.getElementsByClassName('pay-form')[0];
    let fullName = document.getElementById('full-name');
    let cardNumber = document.getElementById('card-number');
    let cvv = document.getElementById('cvv');
    // check validity
    let isFormValid = payForm.checkValidity();  
    if (!isFormValid) {
        payForm.reportValidity()
    }
    else {
        // log out form data and order details.
        e.preventDefault();
        const orderInfo = {
            paymentInfo: {
                fullName: fullName.value,
                cardNumber: cardNumber.value,
                cvv: cvv.value
            },
            orderItems: Object.fromEntries(orderMap)
        };
        console.log(orderInfo);
        
        // clear inputs;
        fullName.value = '';
        cardNumber.value = '';
        cvv.value = '';
    
        // Initial state;
        togglePaymentModal();
        orderMap.clear();
        render();
        renderOrder();
    }
}

// Toggles hidden class on payment modal.
function togglePaymentModal() {
    // display modal
    document.getElementsByClassName('card-details-modal')[0].classList.toggle('hidden');
    // fade background of modal.
    fadeToggle(document.getElementsByClassName('container')[0]);
}

// Removes dishId from orderMap.
function removeDishOrder(dishId) {
    orderMap.delete(dishId);
    hideMinusBtn(dishId);
    renderOrder();
}

// incrementing quantity quantity of dish.id in orderMap
function addToOrderMap(dishId) {
    let quantity = orderMap.get(dishId)
    if(!quantity) {
        orderMap.set(dishId, 1)
    }
    else {
        quantity++;
        orderMap.set(dishId, quantity)
    }
    displayMinusBtn(dishId)
    renderOrder();
}

// decrementing quantity of dish.id in orderMap
function decrementInOrderMap(decrementId) {
    let dishId = decrementId.replace('decrement-', '')
    let quantity = orderMap.get(dishId);
    quantity--;
    if (!quantity) {
        orderMap.delete(dishId);
        hideMinusBtn(dishId);
    }
    else {
        orderMap.set(dishId, quantity)
    };
    renderOrder();
}

// Makes sure that display button hasn't hidden class.
function displayMinusBtn(dishId) {
    let decrementBtn = document.getElementById(`decrement-${dishId}`)
    if (decrementBtn.classList.contains('hidden')) {
        decrementBtn.classList.remove('hidden')
    }
}

// Makes sure that display button has hidden class.
function hideMinusBtn(dishId) {
    let decrementBtn = document.getElementById(`decrement-${dishId}`)
    if (!decrementBtn.classList.contains('hidden')) {
        decrementBtn.classList.add('hidden')
    }
}

// Renders order-menu by getting data from menuArray and orderMap.
function renderOrder() {
    const orderMenu = document.getElementsByClassName('order-menu')[0];
    const orderDetails = document.getElementsByClassName('order-details')[0];
    if (orderMap.size) {
        // Rendering order section.
        let orderHtml = ''; 
        let totalPrice = 0;
        // Looping over menuArray and inside it looping over orderMap (contains ids and quantity of added dished))
        // When ids' of both elements match create html for that order.
        menuArray.forEach(function(dishObj) {
            orderMap.forEach((dishQuantity, dishId, map) => {
                if (dishObj.id === Number(dishId)) {
                    orderHtml += 
                        `
                        <div class="order-dish">
                            <h2>${dishObj.name}</h2>
                            <button class="remove-dish-btn" data-remove-dish="${dishId}">Remove</button>
                            <h2 class="order-dish-price">$${dishObj.price * dishQuantity} (${dishQuantity})</h2>
                        </div>
                        `;
                    totalPrice += dishObj.price * dishQuantity;
                }
            })
        })
        orderHtml += 
            `<div class="total-price">
                <h2>Total price:</h2>
                <h2>$${totalPrice}</h2>
            </div>`;
        orderDetails.innerHTML = orderHtml;
        // Make order section visible.
        orderMenu.classList.remove('hidden');
    }
    else {
        // Create empty order html and hide it.
        orderDetails.innerHTML = '';
        orderMenu.classList.add('hidden');
    }
}

// Renders menu using data from menuArray.
function render() {
    const menuList = document.getElementsByClassName('menu-list')[0];
    let menuHtml = '';

    // render menu-list
    menuArray.forEach(function(dish) {
        let imgSrc = dish.name.toLowerCase().replaceAll(' ', '_');
        menuHtml += `
            <section class="dish-section">
                <img src="./images/${imgSrc}.png" alt="picture of ${dish.name}" class="dish-img">
                <div class="dish-desc">
                    <h2>${dish.name}</h2>
                    <p>${dish.ingredients.join(', ')}</p>
                    <h3>$${dish.price}</h3>
                </div>
                <div class="add-delete-btns">
                    <button class="dish-count-btn add-dish-btn" data-add-dish="${dish.id}">+</button>
                    <button class="dish-count-btn decrement-dish-btn hidden" id="decrement-${dish.id}">-</button>
                </div>
            </section>
        `
    })
    menuList.innerHTML = menuHtml;
}

// Decrease opacity and add grayscale by adding class
function fadeToggle(element) {
    element.classList.toggle('fade')
}
render();
